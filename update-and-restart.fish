#!/usr/bin/env fish

set SCRIPT_DIR (cd (dirname (status --current-filename)); and pwd)

cd $SCRIPT_DIR
git fetch upstream
git rebase upstream/master

cd $SCRIPT_DIR/docker/default
docker build --no-cache -t pluto:latest .

cd $SCRIPT_DIR/docker
docker-compose up -d

echo ""
echo "Don't forget to 'git yolo' to update the fork."
